import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "ballmerpeak"
  val appVersion      = "1.0.1"

  val appDependencies = Seq(
    "mysql" % "mysql-connector-java" % "5.1.21"                                                           //mysql DB driver
    ,"com.typesafe.slick" %% "slick" % "1.0.0"                                                            //slick DB connector
    ,"play" %% "play-test" % "2.1.0"
    ,"play" %% "play-java" % "2.1.0"                                                                      // play.libs.CronExpression
    ,"play" %% "play-jdbc" % "2.1.0"                                                                      // bonecp Connection pool

    ,"securesocial" %% "securesocial" % "master-SNAPSHOT"

//Might come in handy later:
//    ,"org.mindrot" % "jbcrypt" % "0.3m"                                                                 // BCrypt for password storage
    ,"org.scalaz" %% "scalaz-core" % "6.0.4"
//    ,"joda-time" % "joda-time" % "2.2"
//    ,"org.twitter4j" % "twitter4j-core" % "3.0.3"
//    ,"org.twitter4j" % "twitter4j-stream" % "3.0.3"
//    ,"com.googlecode.json-simple" % "json-simple" % "1.1"
//    ,"rome" % "rome" % "1.0"                                                                            //rss reader
//    ,"org.scala-saddle" %% "saddle" % "1.0.1"              //saddle.github.com
//    ,"org.pac4j" % "play-pac4j_java" % "1.1.0-SNAPSHOT"    //https://github.com/leleuj/play-pac4j       //OAuth, google, twitter, linkedin, facebook
                                                                                                          //perhaps use secure social instead http://securesocial.ws/guide/getting-started.html
//    ,"com.loicdescotte.coffeebean" %% "html5tags" % "1.0"                                               //html-5 support for play-generated HTML
    ,"com.restfb" % "restfb" % "1.6.12"                                                                 // Facebook client https://github.com/revetkn/restfb
  )

  val addlResolvers = Seq(
     "Sonatype snapshots repository" at "https://oss.sonatype.org/content/repositories/snapshots/"
     ,Resolver.url("sbt-plugin-snapshots", new URL("http://repo.scala-sbt.org/scalasbt/sbt-plugin-snapshots/"))(Resolver.ivyStylePatterns)
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    resolvers ++= addlResolvers
  )

}
