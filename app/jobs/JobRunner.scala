package jobs

/**
 * Created with IntelliJ IDEA.
 * User: randy
 * Date: 3/19/13
 * Time: 9:41 AM
 */


import play.libs.Time.CronExpression
import akka.actor.{Props, ActorLogging, Actor}
import concurrent.{ExecutionContext, Future}
import services.Config
import java.util.{Date, Calendar}
import util.{Failure, Success}
import play.api.libs.concurrent.Akka
import concurrent.duration.Duration
import java.util.concurrent.TimeUnit
import services.database.DB

object JobRunner extends Config {

  import play.api.Play.current
  import ExecutionContext.Implicits.global
  import play.api.Logger.logger

  val jobRunner = Akka.system.actorOf(Props[JobRunner], name = "Jobs")

  Akka.system.registerOnTermination({
    logger.debug("shutting down JobRunner")  //having this registered seems to prevent hanging in Play??
  })

  def init() {
    if (ConfigValues.jobsRunHere)                  //todo: make configurable
      Akka.system.scheduler.schedule(Duration(0, TimeUnit.SECONDS), Duration(10, TimeUnit.SECONDS), jobRunner, "tick")
  }


}

class JobRunner extends Actor with ActorLogging {

  import com.typesafe.config.ConfigFactory
  import scala.collection.JavaConversions._   //typesafe config library loads values as java collections types
  import DB.db._

  //Check conf for jobs
  val conf = ConfigFactory.load()
  val jobsPrefix = conf.getString("skelly.jobs.jobsPackage")
  val jobSet = (for{
    jobConf <- conf.getConfigList("skelly.jobs.jobs").listIterator()
    kvPair  <- jobConf.entrySet.iterator
  } yield Job(jobsPrefix +"."+ kvPair.getKey, kvPair.getValue.render)).toSet

  implicit val ctx = context.system

  def receive = {
    case a:Any => { //wake up
      //Check db for last run time
      val runtimes:Map[String, Long] = (Jobs.lastRuntimes map (j => j.classname -> j.timestamp.getTime)).toMap //(jobSet.map(x=>x.className).toList)

      jobSet.foreach(job => {
        val lastRun:Long = runtimes.get(job.className).getOrElse(0l)
        val dueDate = job.loadCron.getNextValidTimeAfter(new Date(lastRun))
        //        log.debug("  " + job.className + " due on " + dueDate + "  and now is " + CalendarUtil().getTime)
        if (Calendar.getInstance(JobRunner.ConfigValues.timezone, JobRunner.ConfigValues.locale).getTime.compareTo(dueDate) > -1) { //If last run time is greater than sched
          import ExecutionContext.Implicits.global
          log.info("running " + job.className)
          Future {
            job.run()   //Run that job now, asynch
          } onComplete {
            case Success(_) => log.info("done running...")
            case Failure(failure) => log.error(job.className + " failure:\n" + failure.getMessage)  //todo: better logging for failures
          }
          Jobs.logRuntime(job.className) //update couch
        }
      })
    }
  }

  case class Job(className: String, cronExpression: String) {
    def run() { Class.forName(className).newInstance() }
    lazy val loadCron = {
      val cron = new CronExpression(cronExpression.replaceAll("\"",""))
      cron.setTimeZone(JobRunner.ConfigValues.timezone)
      cron
    }
  }
}