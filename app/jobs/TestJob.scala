package jobs

import java.util.Date
import play.api.Logger

/**
 * Created with IntelliJ IDEA.
 * User: randy
 * Date: 3/19/13
 * Time: 10:43 AM
 */

class TestJob {

  Logger.info("Test job ran on " + (new Date()).toString)
}
