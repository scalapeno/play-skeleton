package services

import java.util.{Locale, TimeZone}
import com.typesafe.config.ConfigFactory
import play.Logger

/**
 * Created with IntelliJ IDEA.
 * User: randy
 * Date: 3/19/13
 * Time: 9:55 AM
 */

trait Config {

  private lazy val configFile = ConfigFactory.load()

  def conf[T](path: String) = try{
    Some(configFile.getAnyRef(path).asInstanceOf[T])
  } catch { case e: Exception => None}

  object ConfigValues {

    lazy val timezone = TimeZone.getTimeZone(("America/Los_Angeles"))
    lazy val locale = new Locale(("en"), ("US"))
    lazy val jobsRunHere = conf[Boolean]("skelly.jobs.runHere").getOrElse(false)

    lazy val dbConnection = conf[String]("skelly.db.connection").getOrElse("direct")
    lazy val dbProfile = conf[String]("skelly.db.profile").getOrElse("dev")
    lazy val dbUser = conf[String](s"skelly.db.$dbProfile.user").getOrElse("")
    lazy val dbPassword = conf[String](s"skelly.db.$dbProfile.password").getOrElse("")
    lazy val dbUrl = conf[String](s"skelly.db.$dbProfile.url").getOrElse("")
    lazy val dbDriver = conf[String](s"skelly.db.$dbProfile.driver").getOrElse("")

  }
}
