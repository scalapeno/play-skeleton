package services.database

import scala.slick.jdbc.meta.MTable
import models._
import play.api.Logger
import services.Config
import slick.driver.{H2Driver, MySQLDriver, ExtendedProfile}
import java.sql.Driver
import com.jolbox.bonecp.BoneCPDataSource
import akka.actor.{ExtendedActorSystem, Extension}
import services.database.DB.{Pooled, Direct}


/**
 * Created with IntelliJ IDEA.
 * User: randy
 * Date: 3/12/13
 * Time: 10:46 AM
 */

object DB extends Config {

  sealed trait ConnectMethod
  case object Pooled extends ConnectMethod
  case object Direct extends ConnectMethod

  lazy val connectMethod = ConfigValues.dbConnection match {
    case "pooled" => {
      Logger.info("Pooled connection")
      Pooled
    }
    case _  => {
      Logger.info("Direct connection")
      Direct
    }
  }

  lazy val db = ConfigValues.dbDriver match {
    case "com.mysql.jdbc.Driver" => {
      Logger.info("starting new MySQL DB connection")
      new DB(new com.mysql.jdbc.Driver, ConfigValues.dbUrl, ConfigValues.dbUser, ConfigValues.dbPassword, ConfigValues.dbDriver, MySQLDriver)
    }

    case "org.h2.Driver" => {
      Logger.info("starting new H2 DB connection")
      new DB(org.h2.Driver.load(), ConfigValues.dbUrl, ConfigValues.dbUser, ConfigValues.dbPassword, ConfigValues.dbDriver, H2Driver)
    }
  }

  def apply() = {
    db.conn    //connection pooling applied within DB class... (ok?)
  }

  def init() {
    db.createTables()
    db.populateTables()
  }
}

class DB (driverClass: Driver, url: String, user: String, pass: String, driver: String, override val profile: ExtendedProfile)
  extends JobComponent
  with MessageComponent
//  with SocialUserComponent
//  with UserComponent
  with Profile
{

  import profile.simple._

  lazy val dataSource = {
    val ds = new BoneCPDataSource()
    ds setJdbcUrl url
    ds setDriverClass driver
    ds setUsername user
    ds setPassword pass
    ds setPartitionCount 1
    ds setMaxConnectionsPerPartition 5
    ds setMinConnectionsPerPartition 1
    ds setStatisticsEnabled true
    ds setStatementsCacheSize 50
    ds setServiceOrder "LIFO"
    ds setConnectionTimeoutInMs 15 * 1000
//    ds setLazyInit true
    ds
  }

//  def conn = {
//    Class.forName(driver)
//    Database forDataSource dataSource
//  }


  def conn = DB.connectMethod match {
    case Direct => Database.forDriver(driverClass, url, user, pass)   //slower, but works on restart.
    case Pooled => Database.forDataSource(dataSource)                 //much faster, but kills play on restart for unknown reason //todo
  }

  def tables = List(
     Jobs
    ,Messages
//    ,Users
  )

  lazy val missingTables = tables.filterNot(t => {
    tableMap.contains(t.tableName.toLowerCase)
  })

  def populateTables() {
//    val tablesWithDefaults = missingTables.collect { case t: Table[_] with DefaultValues[_] => t }
//    tablesWithDefaults.foreach {
//      t => t.insertDefaults //t.insertAll(t.defaultValues.toSeq: _*)
//    }
    import Database.threadLocalSession
    Logger.info("populating tables")
    conn.withSession {
      if(missingTables.filter(t=>t.tableName.toLowerCase=="messages".toLowerCase).size>0) {
        Logger.info("populating messages")
        Messages.insertAll(defaultMessages:_*)    //todo: so ugly
      }
    }

  }

  def createTables() {
    import Database.threadLocalSession
    Logger.info("creating tables for DB")
    conn.withSession {
      missingTables.foreach(m => {
        Logger.info(m.ddl.createStatements.mkString("\n"))
        m.ddl.create
      })
    }
  }


  lazy val tableMap: Map[String, MTable] = {
//    Logger.info(profile.toString)
//    Logger.info(driver)
    import Database.threadLocalSession
    val tableList = conn.withSession {
      MTable.getTables.list()
    }
    val tableMap = tableList.map{t => (t.name.name.toLowerCase, t)}.toMap
    tableMap
  }



}