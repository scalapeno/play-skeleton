package services.database

import scala.slick.driver.H2Driver.simple._
import Database.threadLocalSession
import slick.driver.ExtendedProfile
import play.api.Logger
import collection.mutable.ArrayBuffer

/**
 * Created with IntelliJ IDEA.
 * User: randy
 * Date: 3/29/13
 * Time: 8:37 AM
 */

//trait DefaultValues[T] {
////  self:Table[T] =>
//
//}

trait DefaultValuesComponent[T] {
  val table:Table[T]

  def defaultValues: List[T]

  def insertDefaults() = {
    DB().withSession {
      val vals = defaultValues
      table.insertAll(vals: _*)
    }
  }
}

trait Profile {
  val profile: ExtendedProfile

//  val components = ArrayBuffer.empty[DBComponent]
}

trait DBComponent { //Just a marker
  self: Profile =>

//  self.components += super
}

trait MySQLProfile extends Profile {
  val profile = slick.driver.MySQLDriver
}

trait H2Profile extends Profile {
  val profile = slick.driver.H2Driver
}

//trait AutoCreateComponent extends DBComponent {
//  self: Profile =>
//
//}

//trait DevProfile extends PlayDbProfile {                      //This design is funky because either can be gotten from the path with
//  lazy val SLICK_DRIVER = "ressit.db.dev.driver"
//}
//
//trait ProdProfile extends PlayDbProfile {
//  lazy val SLICK_DRIVER = "ressit.db.prod.driver"
//}

//trait PlayDbProfile extends Profile {
//
//  val SLICK_DRIVER: String
//
////  protected def singleton[T](name : String)(implicit man: Manifest[T]) : T = try {
////    Class.forName(name + "$").getField("MODULE$").get(man.runtimeClass).asInstanceOf[T]
////  } catch {
////    case t: Throwable => {
////      Logger.error("driver error", t)
////      val c = Class.forName(name)
////      c.getField("MODULE$").get(man.runtimeClass).asInstanceOf[T]
////    }
////  }
//
//  lazy val DEFAULT_SLICK_DRIVER = {
//    Logger.error("using default driver")
//    "scala.slick.driver.H2Driver"
//  }
//  lazy val driverClass = play.api.Play.current.configuration.getString(SLICK_DRIVER).getOrElse(DEFAULT_SLICK_DRIVER)
//  lazy val profile = {
//    Logger.info(s"using driver: $driverClass")
//    singleton[ExtendedProfile](driverClass)
//  }
//}
