package services.translate

/**
 * Translate any message into understandable text for a fogie
 */
object FogieTranslator extends TranslateT {
  def translateMessage(message: String): String = {
    translate(message)
  }

  def translateMessageToYouth(message: String): String = {
    translateToYouth(message)
  }

  val youthToFogieWords = scala.collection.immutable.HashSet(
    ("dude", "sonny"),
    ("bitch", "nag"),
    ("douche", "wet blanket"),
    ("jerk", "hooligan"),
    ("kinda like", "love")
  )

  private def translate(message: String): String = {
    var translatedMessage = message
    youthToFogieWords foreach { words =>
      translatedMessage = translatedMessage replaceAll (words._1, words._2)
    }
    translatedMessage
  }

  private def translateToYouth(message: String): String = {
    var translatedMessage = message
    youthToFogieWords foreach { words =>
      translatedMessage = translatedMessage replaceAll (words._2, words._1)
    }
    translatedMessage
  }
}
