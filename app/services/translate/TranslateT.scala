package services.translate

trait TranslateT {
  def translateMessage(message: String): String
}
