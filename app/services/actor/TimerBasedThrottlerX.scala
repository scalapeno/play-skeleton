//package services.actor
//
//import akka.actor.{ActorRef, FSM, Actor}
//import concurrent.duration.Duration
//import services.actor.A.{Active, Idle}
//import scala.collection.immutable.{ Queue => Q }
//
///**
// * Created with IntelliJ IDEA.
// * User: randy
// * Date: 3/29/13
// * Time: 4:06 PM
// */
//
//case class Rate(val numberOfCalls: Int, val duration: Duration) {
//  /**
//   * The duration in milliseconds.
//   */
//  def durationInMillis(): Long = duration.toMillis
//}
//
//case class SetTarget(target: Option[ActorRef])
//case class Queue(msg: Any)
//
//
//object A {
//
//  sealed trait State
//  case object Idle extends State
//  case object Active extends State
//
//
//
//  sealed case class Data(
//                          target: Option[ActorRef],
//                          vouchersLeft: Int,
//                          queue: Q[Any]
//                          )
//}
//
//class TimerBasedThrottler(var rate: Rate) extends Actor with FSM[A.State, A.Data] {
//  startWith(Idle, A.Data(None, rate.numberOfCalls, Q[Any]()))
//
//  when(Idle) {
//    // To do
//  }
//
//  when(Active) {
//    // To do
//  }
//
//  onTransition {
//    case Idle -> Active => setTimer("moreVouchers", Tick, rate.duration, repeat = true)
//    case Active -> Idle => cancelTimer("moreVouchers")
//  }
//
//  initialize
//}
//
//// When active, the timer will send Tick messages to the throttler
//case object Tick
