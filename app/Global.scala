import jobs.JobRunner
import play.api.test.FakeApplication
import play.api.{Logger, Application, GlobalSettings}
import services.database.DB

/**
 * Created with IntelliJ IDEA.
 * User: randy
 * Date: 2/26/13
 * Time: 10:33 AM
 */

object Global extends GlobalSettings {

  override def onStart(app: Application) {

    Logger.info("app start")

    DB.init()

    JobRunner.init()

  }

  override def onStop(app: Application) {

    Logger.info("closing DB connection")
    DB.db.dataSource.close()

  }

  val fake = FakeApplication()

}