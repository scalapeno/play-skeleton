package models

import java.sql.Timestamp
import services.database.{DB, Profile}
import play.api.Logger
import slick.jdbc.{StaticQuery => Q, GetResult}

/**
 * Created with IntelliJ IDEA.
 * User: randy
 * Date: 4/5/13
 * Time: 2:02 PM
 */

case class Message(sender: String, message: String, timestamp: Timestamp)

trait MessageComponent {
  self: Profile =>

  Logger.info("init MessageComp")

  import profile.simple._
  import Database.threadLocalSession

  object Messages extends Table[Message]("messages".toUpperCase) {
    def sender =       column[String]("sender".toUpperCase)
    def message =      column[String]("message".toUpperCase)
    def timestamp =    column[Timestamp]("TIMESTAMP".toUpperCase)
    def * = sender ~ message ~ timestamp <>(Message, Message.unapply _)

    implicit val getResult = GetResult(r => Message(r.<<, r.<<, r.<<))

    def all: List[Message] = DB().withSession {
      val q = for{
        m <- Messages
      } yield m
      q.list()
    }

    def saveMessage(sender: String, message: String) = DB().withSession {
      Messages.insert(Message(sender, message, new Timestamp(System.currentTimeMillis)))
    }

  }

  val defaultMessages = List (
    Message("randy", "Hi there", new Timestamp(System.currentTimeMillis()))

  )


}
