package models

import services.database.{DB, Profile}
import play.api.Logger
import slick.jdbc.{StaticQuery => Q, GetResult}
import org.mindrot.jbcrypt.BCrypt

/**
 * Created with IntelliJ IDEA.
 * User: randy
 * Date: 4/4/13
 * Time: 8:21 AM
 */

object Role {
  def apply(s: String): Role = s match {
    case "A" => Admin
    case _ => Plain
  }
}
sealed abstract class Role(val symbol: String)
case object Plain extends Role("P")
case object Admin extends Role("A")

case class User(email: String, passwordHash: String, role: Role)

trait UserComponent {
  self: Profile =>

  Logger.info("init UserComponent")

  import profile.simple._
  import Database.threadLocalSession

  object Users extends Table[User]("USERS".toUpperCase) {
    def email =          column[String]("EMAIL".toUpperCase)
    def passwordHash =   column[String]("PASSWORDHASH".toUpperCase)
    def role =           column[Role]("ROLE".toUpperCase)
    def * = email ~ passwordHash ~ role <>(User, User.unapply _)

    implicit val userResult = GetResult(r => User(r.<<, r.<<, Role(r.<<)))

    implicit val roleTypeMapper = MappedTypeMapper.base[Role, String]( _.symbol, Role(_) )

    def withEmailAndPassword(email: String, rawPassword: String): Option[User] = {
      withEmail(email).filter(user => BCrypt.checkpw(rawPassword, user.passwordHash))
    }

    def withEmail(email: String): Option[User] = DB().withSession {
      val q = for {
        user <- Users if(user.email === email )
      } yield user
      q.list.headOption
    }

    def add(email: String, rawPassword: String, role: Role = Plain) = DB().withSession {
      val hashedPassword = BCrypt.hashpw(rawPassword, BCrypt.gensalt)
      Users.insert(User(email, hashedPassword, role))
    }
  }

  def defaultUsers: List[User] = List (
     User("randy.unger@gmail.com", BCrypt.hashpw("rrrrrr", BCrypt.gensalt), Admin)
    ,User("treydecamp@gmail.com", BCrypt.hashpw("123456", BCrypt.gensalt), Admin)
    ,User("jungjinah@gmail.com", BCrypt.hashpw("123456", BCrypt.gensalt), Admin)
  )
}