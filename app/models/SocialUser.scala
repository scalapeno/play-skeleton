//package models
//
//import services.database.{DB, Profile}
//import play.Logger
//import scala.slick.jdbc.GetResult
//import org.mindrot.jbcrypt.BCrypt
//import securesocial.core._
//import securesocial.core.UserId
//import securesocial.core.OAuth2Info
//import securesocial.core.OAuth1Info
//import securesocial.core.PasswordInfo
//
///**
// * Created with IntelliJ IDEA.
// * User: Randy
// * Date: 4/4/13
// * Time: 7:33 PM
// */
//
////object Role {
////  def apply(s: String): Role = s match {
////    case "A" => Admin
////    case _ => Plain
////  }
////}
////sealed abstract class Role(val symbol: String)
////case object Plain extends Role("P")
////case object Admin extends Role("A")
//
//case class SocialUser(id: UserId, firstName: String, lastName: String, fullName: String, email: Option[String],
//                      avatarUrl: Option[String], authMethod: AuthenticationMethod,
//                      oAuth1Info: Option[OAuth1Info] = None,
//                      oAuth2Info: Option[OAuth2Info] = None,
//                      passwordInfo: Option[PasswordInfo] = None) extends Identity
//
//trait SocialUserComponent {
//  self: Profile =>
//
//  Logger.info("init SocialUserComponent")
//
//  import profile.simple._
//  import Database.threadLocalSession
//
//  object Users extends Table[SocialUser]("SOCIALUSERS".toUpperCase) {
//    def id
//    def email =          column[String]("EMAIL".toUpperCase)
//    def passwordHash =   column[String]("PASSWORDHASH".toUpperCase)
//    def role =           column[Role]("ROLE".toUpperCase)
//    def * = email ~ passwordHash ~ role <>(SocialUser, SocialUser.unapply _)
//
//    implicit val userResult = GetResult(r => User(r.<<, r.<<, Role(r.<<)))
//
//    implicit val roleTypeMapper = MappedTypeMapper.base[Role, String]( _.symbol, Role(_) )
//
//    def withEmailAndPassword(email: String, rawPassword: String): Option[User] = {
//      withEmail(email).filter(user => BCrypt.checkpw(rawPassword, user.passwordHash))
//    }
//
//    def withEmail(email: String): Option[User] = DB().withSession {
//      val q = for {
//        user <- Users if(user.email === email )
//      } yield user
//      q.list.headOption
//    }
//
//    def add(email: String, rawPassword: String, role: Role = Plain) = DB().withSession {
//      val hashedPassword = BCrypt.hashpw(rawPassword, BCrypt.gensalt)
//      Users.insert(User(email, hashedPassword, role))
//    }
//  }
//
//  def defaultUsers: List[User] = List (
//     User("randy.unger@gmail.com", BCrypt.hashpw("rrrrrr", BCrypt.gensalt), Admin)
//    ,User("treydecamp@gmail.com", BCrypt.hashpw("123456", BCrypt.gensalt), Admin)
//    ,User("jungjinah@gmail.com", BCrypt.hashpw("123456", BCrypt.gensalt), Admin)
//  )
//}
