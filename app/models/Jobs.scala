package models

import services.database.{Profile, DB}
import java.sql.Timestamp
import scala.slick.jdbc.{StaticQuery => Q, GetResult}
import play.api.Logger

/**
 * Created with IntelliJ IDEA.
 * User: randy
 * Date: 3/19/13
 * Time: 9:45 AM
 */

case class Job(classname: String, timestamp: Timestamp)

trait JobComponent {
  self: Profile =>

  Logger.info("init JobComponent")

  import profile.simple._
  import Database.threadLocalSession

  object Jobs extends Table[Job]("JOBS".toUpperCase) {
    def classname =    column[String]("CLASSNAME".toUpperCase)
    def timestamp = column[Timestamp]("TIMESTAMP".toUpperCase)
    def * = classname ~ timestamp <>(Job, Job.unapply _)

    implicit val getJobResult = GetResult(r => Job(r.<<, r.<<))

    def lastRuntimes: List[Job] = {
      val query = Q.queryNA[Job](
        """
          |select * from JOBS j
          |inner join (
          |  select CLASSNAME, MAX(TIMESTAMP) as TIMESTAMP from JOBS
          |  group by CLASSNAME
          |) c on j.CLASSNAME = c.CLASSNAME
          |""".stripMargin)
      DB().withSession(query.list())
    }

    def logRuntime(classname: String) = DB().withSession {
      Jobs.insert(Job(classname, new Timestamp(System.currentTimeMillis)))
    }

  }
}