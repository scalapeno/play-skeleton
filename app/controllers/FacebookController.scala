package controllers

import services.translate.FogieTranslator
import services.facebook.FacebookService

import play.api.data._
import play.api.data.Forms._
import play.api.mvc._

object FacebookController extends Controller with securesocial.core.SecureSocial {
  def post = Action {
    Ok(views.html.post(""))
  }

  def youthMessage(message: String) = SecuredAction {
    implicit request =>
    val tm = FogieTranslator.translateMessage(message)
    FacebookService(request.user.oAuth2Info.get).publish(tm)
    // TODO encode this in JSON before sending back
    Ok(s"""Message was successfully translated to fogie-speak and posted on Facebook
>>>$tm
      """)
  }

  def youthFeed = Action {
    Ok("Youth You got a list of JSON, believe it")
  }

  def fogieMessage(message: String) = SecuredAction {
    implicit request =>
    val tm = FogieTranslator.translateMessageToYouth(message)
    FacebookService(request.user.oAuth2Info.get).publish(tm)
    // TODO encode this in JSON before sending back
//    Ok(s"""Message was successfully translated to youth-speak and posted on Facebook
//>>>$tm
//      """)
    Ok(views.html.post(s"$tm submitted"))
  }

  def fogieFeed = Action {
    Ok("Fogie You got a list of JSON, believe it")
  }
}
