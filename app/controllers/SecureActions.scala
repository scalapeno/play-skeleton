//package controllers
//
//import play.api.mvc._
//
///**
// * Created with IntelliJ IDEA.
// * User: randy
// * Date: 4/4/13
// * Time: 8:18 AM
// */
//
//trait SecureActions extends Controller {
//
//  def LoggedInAction(implicit action: RequestHeader => Result) = FilteredAction {
//    implicit request => isLoggedIn
//  }
//
//  def AdminAction(implicit action: RequestHeader => Result) = FilteredAction {
//    implicit request => isLoggedIn && isAdmin
//  }
//
//  def FilteredAction(filter: RequestHeader => Boolean)(implicit action: RequestHeader => Result) = Action {
//    implicit request => filter(request) match {
//      case true => action(request)
//      case false => Unauthorized
//    }
//  }
//
//  private def isLoggedIn(implicit request: RequestHeader) = request.session.get(Security.username).isDefined
//  private def isAdmin(implicit request: RequestHeader) = request.session.get("role").filter(_.contains("A")).isDefined
//
//  def username(implicit request: RequestHeader) = request.session(Security.username)
//
//}