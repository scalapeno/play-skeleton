package controllers

import play.api.mvc.{AnyContent, Controller}
import services.database.DB
import play.api.data.Form
import play.api.data.Forms._
import play.api.http.{ContentTypes, ContentTypeOf, Writeable}

import scalaz._
import Scalaz._

/**
 * Created with IntelliJ IDEA.
 * User: randy
 * Date: 4/5/13
 * Time: 2:17 PM
 */

object MessagesController extends Controller with securesocial.core.SecureSocial {

  import DB.db.Messages

  //Anyone can see messages, but only logged in users can post

  def viewAll = UserAwareAction {
    implicit request => {

      val out = new StringBuilder
      out.append(s"messages for $firstName:\n")
      out.append(Messages.all.mkString("\n"))

      Ok(views.html.messages(firstName, Messages.all))
    }
  }

  def firstName(implicit request: RequestWithUser[AnyContent]) = {
    request.user map (_.firstName) getOrElse "Guest"
  }


  val postMessageForm = Form(
    mapping(
      "sender" -> nonEmptyText
      ,"message"        -> nonEmptyText
    )(PostMessageForm.apply)(PostMessageForm.unapply))  //verification is performed later

  case class PostMessageForm(sender: String, message: String)

  def postMessage = SecuredAction(ajaxCall = true) {
    implicit request => {
      postMessageForm.bindFromRequest.fold (
        formWithErrors => BadRequest(formWithErrors.errorsAsJson.toString).as(ContentTypes.JSON)
        ,form          => save(form.sender, form.message).fold(
          exception       => BadRequest("no naughty words"),
          successMsg      => Ok("")
        )
      )
    }
  }

  def save(sender: String, message: String): Validation[Exception, String] = {
    if (message.matches("d*mn")) new IllegalArgumentException("potty mouth").fail
    else {
      Messages.saveMessage(sender, message)
      message.success
    }
  }

//View docs located here
  //http://www.playframework.com/documentation/api/2.0/scala/index.html#views.html.helper.package

//  protected implicit val wValidationStringAny = Writeable[Validation[String, Any]](e => {
//    e.fold(stringToByteArray, anyToByteArray)
//  })
//
//  protected implicit val cRightAny = ContentTypeOf[Validation[String, Any]](Some(ContentTypes.JSON))

}
