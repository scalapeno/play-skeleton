//package controllers
//
//import play.api.mvc.{Action, Security, Controller}
//import services.database.DB
//import models.User
//
///**
// * Created with IntelliJ IDEA.
// * User: randy
// * Date: 4/4/13
// * Time: 8:20 AM
// */
//
//object Login extends Controller {
//
//  import DB.db._
//
//  def login(email: Option[String], pwd: Option[String]) = Action {
//    val maybeUser: Option[User] = for {
//      u <- email
//      p <- pwd
//      validUser <- Users.withEmailAndPassword(u, p)
//    } yield validUser
//
//
//    maybeUser match {
//      case Some(user) => Ok.withSession(Security.username -> user.email, "role" -> user.role.symbol)
//      case None => BadRequest
//    }
//  }
//
//}
//
