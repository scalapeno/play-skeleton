package controllers

import play.api.mvc._

//import models.{Articles, Feeds, Jobs}
import services.database.DB

object Application extends Controller with securesocial.core.SecureSocial {

  import DB.db._

  def index = Action {
    //Ok("hello world").as("text/html")
    Ok(views.html.index())
  }

  def user = SecuredAction {
    implicit request => Ok(views.html.user(request.user))
  }

}
